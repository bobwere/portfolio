const express = require("express");
const getUser = require("./utils/helpers");

const app = express();

app.get("/", (req, res) => {
  return res.json(getUser());
});

app.listen(8080, () => {
  console.log("Listening on port 8080..");
});
