const getUser = require("../utils/helpers");
var assert = require("assert");

describe("test utils", function () {
  it("should return json with user details when getUser is called", function () {
    const user = getUser();

    assert.equal(user.name, "Bob Were");
  });
});
